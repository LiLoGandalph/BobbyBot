{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}

--------------------------------------------------------------------------------

module Types where

--------------------------------------------------------------------------------

import qualified Database.SQLite.Simple as DB

import Network.WebSockets

import Data.Aeson
import Data.Aeson.Types

import Data.Text (pack)

import Data.Time.Clock.POSIX (posixSecondsToUTCTime, POSIXTime)
import Data.Time (UTCTime)

import Data.Maybe (isJust, fromJust)
import Control.Monad ((>=>))

-------------------------------------------------------------------------------

type Price = Double

type Symbol = String

type Indicator = Connection -> Symbol -> IO (Maybe ContractBP)

data Strategy = Strategy { isBuying           :: POSIXTime -> Bool
                         , succDelay          :: Int
                         , failDelay          :: Int
                         , indicator          :: Indicator
                         , symbols            :: [Symbol]
                         , config             :: Config 
                         , isTransactionCheck :: POSIXTime -> Bool }

-------------------------------------------------------------------------------

data Basis = Stake | Payout

instance Show Basis where
  show Stake  = "stake"
  show Payout = "payout"


data Config = Config { amount         :: Double
                     , basis          :: Basis
                     , expiryDateCalc :: IO Int }

-------------------------------------------------------------------------------

data ContractType = Lower | Higher deriving Eq

instance Show ContractType where
  show Lower  = "PUT"
  show Higher = "CALL"

  
data ContractBP = ContractBP { contractBPType   :: ContractType
                             , contractBPSymbol :: Symbol
                             , contractBPDesc   :: String
                             } deriving (Eq, Show)
        

instance FromJSON ContractBP where
  parseJSON = withObject "contract BP" $ \c -> do
    contractBPSymbol   <- c .: pack "symbol"
    contractBPType     <- (\s -> if s == ("PUT" :: String) then Lower else Higher) <$> (c .: pack "contract_type")
    let contractBPDesc = ""
    return ContractBP{..}


data Contract = Contract { contractType   :: ContractType
                         , contractAmount :: Double
                         , contractBasis  :: Basis
                         , contractSymbol :: Symbol
                         , contractDesc   :: String
                         , expiryDate     :: Int
                         } deriving Show


instance ToJSON Contract where
  toJSON Contract{..} =
    object [ "buy"   .= pack "1"
           , "price" .= contractAmount
           , "parameters" .= object [ "amount"        .= contractAmount
                                    , "basis"         .= (pack.show) contractBasis
                                    , "contract_type" .= (pack.show) contractType
                                    , "currency"      .= pack "USD"
                                    , "date_expiry"   .= expiryDate
                                    , "symbol"        .= pack contractSymbol ] ]

-------------------------------------------------------------------------------

contractID :: Value -> Parser String
contractID = withObject "`buy` response" ((.: "buy") >=> (.: "contract_id"))

-------------------------------------------------------------------------------

data User = User { email    :: String
                 , balance  :: Double
                 , currency :: String
                 } deriving (Show)

instance FromJSON User where
  parseJSON = withObject "user" $ \o -> do
    auth     <- o .: pack "authorize"
    email    <- auth .: pack "email"
    balance  <- read <$> (auth .: pack "balance")
    currency <- auth .: pack "currency"
    return User{..}

-------------------------------------------------------------------------------

data HistoryRequest = HistoryRequest { historySymbol :: Symbol
                                     , count         :: Int
                                     , granularity   :: Int
                                     , end           :: Maybe Int
                                     } deriving (Show)

instance ToJSON HistoryRequest where
  toJSON HistoryRequest{..} = 
    object [ "ticks_history" .= pack historySymbol
           , "end"           .= if isJust end then (pack . show . fromJust) end else pack "latest"
           , "count"         .= (10 * count)
           , "granularity"   .= granularity
           , "style"         .= pack "candles"
           ]

-------------------------------------------------------------------------------

data Transaction = Transaction { transactionID     :: Int
                               , transactionSymbol :: Symbol
                               , buyPrice          :: Price
                               , sellPrice         :: Price
                               , purchaseTime      :: UTCTime
                               , sellTime          :: UTCTime
                               , shortcode         :: ContractType
                               , profit            :: Price
                               , win               :: Bool
                               , potentialPayout   :: Double
                               } deriving Show


instance FromJSON Transaction where
  parseJSON = withObject "transaction" $ \t -> do
    transactionID     <- read <$> (t .: "contract_id")
    buyPrice          <- read <$> (t .: "buy_price")
    sellPrice         <- read <$> (t .: "sell_price")
    purchaseTime      <- (posixSecondsToUTCTime.realToFrac.read) <$> (t .: "purchase_time")
    sellTime          <- posixSecondsToUTCTime <$> (t .: "sell_time")
    shortcode         <- (\s -> if (takeWhile (/= '_') s) == "PUT" then Lower else Higher) <$> (t .: "shortcode")
    transactionSymbol <- (takeWhile (/= '_').drop 1.dropWhile (/= '_')) <$> (t .: "shortcode")
    payout            <- read <$> (t .: "payout")
    let profit        =  sellPrice - buyPrice
        win           =  profit > 0
        potentialPayout = (payout - buyPrice) / buyPrice * 100
    return Transaction{..}


transactions :: Value -> Parser [Transaction]
transactions = withObject "profit table response" ((.: "profit_table") >=> (.: "transactions")) 


instance DB.ToRow Transaction where
  toRow Transaction{..} = DB.toRow (show purchaseTime, show sellTime, show potentialPayout, win, profit, transactionID)