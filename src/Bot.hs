{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE PartialTypeSignatures #-}

--------------------------------------------------------------------------------

module Bot where

--------------------------------------------------------------------------------

import Types

import Network.WebSockets
import Wuss (runSecureClient)

import Data.Aeson
import Data.Aeson.Types

import Data.Monoid ((<>))
import Data.Char (toUpper)
import Data.Text (Text, pack, unpack)
import Control.Monad (forever, void)
import Control.Applicative (pure)
import Control.Concurrent (threadDelay)
import Control.Exception (try)

import Control.Concurrent.Async (mapConcurrently_)

import Data.Time (getCurrentTime)
import Data.Time.Clock.POSIX (getPOSIXTime, posixSecondsToUTCTime)

import qualified Database.SQLite.Simple as DB

--------------------------------------------------------------------------------

_dbName_ :: String
_dbName_ = "C:\\Users\\light\\OneDrive\\Documents\\Databases\\Trades.db"

--------------------------------------------------------------------------------

authorize :: Connection -> IO ()
authorize connection = do
    sendTextData connection $ encode $ object [(pack "authorize") .= (pack "VXRRiifFqg02MMV")]
    void $ (receiveData connection :: IO Text)


createContract :: Config -> ContractBP -> IO Contract
createContract Config{..} ContractBP{..} =
    (Contract contractBPType amount basis contractBPSymbol contractBPDesc) <$> expiryDateCalc


buyContract :: Connection -> Contract -> IO ()
buyContract connection contract = sendTextData connection $ encode contract


trySymbol :: Connection -> Config -> Indicator -> Symbol -> IO ()
trySymbol connection config indicator symbol = do
    contractBP <- indicator connection symbol
    contract <- traverse (createContract config) contractBP
    maybe nothing buying contract
    where nothing = putStrLn $ symbol <> " - nothing interesting("
          buying cont = do
            buyContract connection cont
            response <- receiveData connection
            let mbID = read <$> ((parseMaybe contractID) =<< (decode response))
            maybe
                (putStrLn $ "Failed buying " <> show cont <> "\nSomething wrong with parsing this: " <> show response)
                (\id -> do
                    addBuyDBRecord id cont
                    putStrLn $ show symbol <> " -- bought some shit: " <> show cont <> "\nContract ID: " <> show id)
                mbID


runStrategy :: Strategy -> IO ()
runStrategy Strategy{..} =
            openConnection $ \connection -> do
                putStrLn "Connection opened."
                authorize connection
                traverse (trySymbol connection config indicator) symbols
                closeConnection connection
                putStrLn "Connection closed."


bot :: Strategy -> IO ()
bot strategy@Strategy{..} = forever $ do
    time <- getPOSIXTime
    print $ posixSecondsToUTCTime time

    if isTransactionCheck time
        then do
            putStrLn "Time to count some money.."
            mbTransactions <- openConnection getLatestTransactions
            maybe (pure ()) (void.traverse addResultDBRecord) mbTransactions
            threadDelay succDelay
        else pure ()

    if isBuying time
        then do
             putStrLn "It's nice time to fuck Binary.."
             runStrategy strategy
             threadDelay succDelay
        else threadDelay failDelay


openConnection :: ClientApp a -> IO a
openConnection app = do
    resp <- try $ runSecureClient "ws.binaryws.com" 443 "/websockets/v3?app_id=12368" app :: IO (Either HandshakeException _)
    case resp of
        Left ex -> do
            print ex
            time <- getCurrentTime
            appendFile "Log.txt" ("++++ openConnection: " <> show ex <> " :==: " <> show time <> "\n")
            openConnection app
        Right res ->
            return res

closeConnection :: Connection -> IO ()
closeConnection connection = do
    sendClose connection (pack "Bye")
    resp <- try $ receiveData connection :: IO (Either ConnectionException Text)
    case resp of
        Left ex -> print ex
        Right res -> print res

-------------------------------------------------------------------------------

profitTableRequest :: Connection -> Int -> IO ()
profitTableRequest connection dateFrom =
    sendTextData connection $ encode $ object [ (pack "profit_table") .= (1 :: Int)
                                              , (pack "description")  .= (1 :: Int)
                                              , (pack "date_from")    .= (pack.show) dateFrom
                                              , (pack "sort")         .= (pack "ASC")]


getLatestTransactions :: Connection -> IO (Maybe [Transaction])
getLatestTransactions connection = do
    dateFrom <- (subtract (3600).truncate) <$> getPOSIXTime
    authorize connection
    profitTableRequest connection dateFrom
    profitTableMsg <- receiveData connection
    closeConnection connection
    pure $ (decode profitTableMsg) >>= (parseMaybe transactions)

-------------------------------------------------------------------------------

addBuyDBRecord :: Int -> Contract -> IO ()
addBuyDBRecord id Contract{..} = do
    db <- DB.open _dbName_
    DB.execute_ db (DB.Query $ pack "CREATE TABLE IF NOT EXISTS trades (id INTEGER PRIMARY KEY, symbol TEXT, purchase_time TEXT, sell_time TEXT, contract_type TEXT, buy_price DOUBLE, potential_payout DOUBLE, win BOOLEAN, profit_loss DOUBLE, description TEXT DEFAULT \"\")")
    DB.execute  db (DB.Query $ pack "INSERT INTO trades (id, symbol, contract_type, buy_price, description) VALUES (?, ?, ?, ?, ?)") (id, map toUpper contractSymbol, show contractType, contractAmount, contractDesc)
    DB.close db


addResultDBRecord :: Transaction -> IO ()
addResultDBRecord transaction = do
    db <- DB.open _dbName_
    DB.execute db (DB.Query $ pack "UPDATE trades SET purchase_time = ?, sell_time = ?, potential_payout = ?, win = ?, profit_loss = ? where id = ?") transaction
    DB.close db
    putStrLn $ show transaction