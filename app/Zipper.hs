module Zipper where

-------------------------------------------------------------------------------

data Zipper a = Zipper { before  :: ![a]
                       , current :: !a
                       , after   :: ![a]
                       } deriving (Show, Eq)

-------------------------------------------------------------------------------

fromList :: [a] -> Maybe (Zipper a)
fromList []     = Nothing
fromList (x:xs) = Just $ Zipper { before  = []
                                , current = x
                                , after   = xs }


toList :: Zipper a -> [a]
toList zipper =
  (( reverse . before) zipper)
  ++ [current zipper]
  ++ (after zipper)


prev :: Zipper a -> Maybe (Zipper a)
prev zipper = if ((null . before) zipper)
  then Nothing
  else Just $ Zipper { before  = before'
                     , current = current'
                     , after   = after' }
    where
      before'  = tail $ before zipper
      current' = head $ before zipper
      after'   = (current zipper) : (after zipper)


next :: Zipper a -> Maybe (Zipper a)
next zipper = if ((null . after) zipper)
  then Nothing
  else Just $ Zipper { before  = before'
                     , current = current'
                     , after   = after' }
    where
      before'  = (current zipper) : (before zipper)
      current' = head $ after zipper
      after'   = tail $ after zipper
