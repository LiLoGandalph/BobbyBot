{-# LANGUAGE OverloadedStrings #-}

-------------------------------------------------------------------------------

module PivotIndicator {-(pivotIndicator)-} where

-------------------------------------------------------------------------------

import Zipper
import Types
import Pivots

import Network.WebSockets (receiveData, sendTextData, Connection)
import Wuss

import Control.Monad ((>=>))

import Data.Aeson
import Data.Aeson.Types

import Data.Monoid ((<>))
import Data.List (partition, isInfixOf)
import Data.Maybe (mapMaybe, fromJust, isJust)

import Control.Lens ((^.))

import Data.Time.Clock.POSIX (getPOSIXTime)

-------------------------------------------------------------------------------

pivotIndicator :: Indicator
pivotIndicator connection symbol = do
    -- Getting 1-hour candles for last 3 days.
    candles <- getHistory connection $ HistoryRequest { historySymbol = symbol
                                                      , count         = 72
                                                      , granularity   = 3600
                                                      , end           = Nothing }
    -- Finding pivots.
    let pivots = (normalize symbol . uncurry (++) . findPivots) candles
    -- Getting last 5-minute candle.
    lastCandle <- last <$> getHistory connection HistoryRequest { historySymbol = symbol
                                                                , count         = 1
                                                                , granularity   = 300
                                                                , end           = Nothing }
    -- Looking for pivot penetrated by last 5-minute candle.
    let penetrations = mapMaybe (penetrate lastCandle) pivots

    case penetrations of
        [] -> pure Nothing
        ((contrType, pivot):_) -> do
            -- Getting current contracts
            contracts <- portfolioRequest connection
            -- Checking if there are any contracts open in current hour.
            if ((ContractBP contrType symbol "") `elem` contracts)           ||
               ((ContractBP (inverse contrType) symbol "") `elem` contracts) ||
            -- Checking if previous 1-hour candle penetrates the same pivot.
               (isJust (penetrate (last candles) pivot))

            then pure Nothing
            else pure $ Just $ ContractBP contrType symbol pivotDesc

            where inverse t = if t == Higher
                              then Lower
                              else Higher

                  pivotDesc = show (pivot^.pivotType)
                            <> " pivot: "
                            <> show (pivot^.pivotPrice)
                            <> ".\nFull candle info:\n"
                            <> show (pivot^.pivotCandle)
            
-------------------------------------------------------------------------------

penetrate :: Candle -> Pivot -> Maybe (ContractType, Pivot)
penetrate candle pivot =
    let open'       = candle^.open
        close'      = candle^.close
        pivotPrice' = pivot^.pivotPrice
    in  if close' - open' > 0
        then if open' < pivotPrice' && pivotPrice' < close'
             then Just (Higher, pivot)
             else Nothing
        else if close' < pivotPrice' && pivotPrice' < open'
             then Just (Lower, pivot)
             else Nothing

-------------------------------------------------------------------------------

portfolioRequest :: Connection -> IO [ContractBP]
portfolioRequest connection = do
  sendTextData connection $ encode $ object ["portfolio" .= (1 :: Int)]
  portfolioMsg <- receiveData connection
  (pure.fromJust) $ (decode portfolioMsg) >>= (parseMaybe contractBPs)


contractBPs :: Value -> Parser [ContractBP]
contractBPs = withObject "portfolio response" ((.: "portfolio") >=> (.: "contracts"))

-------------------------------------------------------------------------------

getHistory :: Connection -> HistoryRequest -> IO [Candle]
getHistory connection hr = do
    sendTextData connection (encode hr)
    historyMsg <- receiveData connection
    let rawCandles = (init . fromJust) $ (parseMaybe (withObject "history response" (.: "candles"))) =<< (decode historyMsg)
    pure $ drop ((subtract (count hr) . length) rawCandles) rawCandles

-------------------------------------------------------------------------------

normalize :: Symbol -> [Pivot] -> [Pivot]
normalize _ [] = []
normalize symbol (x:xs) =
    let pipsDelta = if "JPY" `isInfixOf` symbol then 0.1 else 0.001
        (same, dif) = partition (\y -> abs (x^.pivotPrice - y^.pivotPrice) < pipsDelta) xs
        avg ls = (\(s, n) -> s / n) $ foldl (\(s, n) x -> (s + x^.pivotPrice, n + 1)) (0, 0) ls
    in  (Pivot {_pivotPrice = avg (x:same), _pivotType = x^.pivotType, _pivotCandle = x^.pivotCandle}):(normalize symbol dif)