{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

-------------------------------------------------------------------------------

module Pivots where

-------------------------------------------------------------------------------

import Types
import Zipper

import Data.Aeson
import Data.Aeson.Types

import Data.Time.Clock.POSIX (posixSecondsToUTCTime)

import Control.Lens

-------------------------------------------------------------------------------

_range_ = 10 :: Int

-------------------------------------------------------------------------------

data Candle = Candle { _time  :: Int
                     , _open  :: Price
                     , _high  :: Price
                     , _low   :: Price
                     , _close :: Price
                     } deriving Eq

makeLenses ''Candle

instance Show Candle where
  show candle = "Time: " ++ (show.posixSecondsToUTCTime.realToFrac) (candle^.time) ++ "\nOpen: " ++ show (candle^.open) ++ "\nClose: " ++ show (candle^.close) ++ "\nHigh: " ++ show (candle^.high) ++ "\nLow: " ++ show (candle^.low)

instance FromJSON Candle where
  parseJSON = withObject "candle" $ \candle -> do
    _time  <- (candle .: "epoch")
    _open  <- read <$> (candle .: "open")
    _high  <- read <$> (candle .: "high")
    _low   <- read <$> (candle .: "low")
    _close <- read <$> (candle .: "close")
    return Candle{..}
  
getCandles :: Value -> Parser [Candle]
getCandles = withObject "history response" (.: "candles")

-------------------------------------------------------------------------------

data PivotType = Low | High deriving (Show, Eq)

data Pivot = Pivot { _pivotType   :: PivotType
                   , _pivotPrice  :: Price
                   , _pivotCandle :: Candle
                   } deriving (Show, Eq)
                  
makeLenses ''Pivot

-------------------------------------------------------------------------------

findPivots :: [Candle] -> ([Pivot], [Pivot])
findPivots candles =
  let lowz  = fromList [(c^.low,  c) | c <- candles]
      highz = fromList [(c^.high, c) | c <- candles]
  in  (reverse $ findPivots' Low lowz [], reverse $ findPivots' High highz [])


findPivots' :: PivotType -> Maybe (Zipper (Price, Candle)) -> [Pivot] -> [Pivot]
findPivots' _ Nothing foundPivots = foundPivots
findPivots' pivotType (Just prices) foundPivots =
  if checkPivot pivotType prices
    then findPivots' pivotType updPrices ((Pivot pivotType ((fst.current) prices) ((snd.current) prices)) : foundPivots)
    else findPivots' pivotType updPrices foundPivots
    where updPrices = next prices


checkPivot :: PivotType -> Zipper (Price, Candle) -> Bool
checkPivot pivotType prices =
  let bef = ((map fst).(take _range_).before) prices
      aft = ((map fst).(take _range_).after)  prices
      cur = (fst.current) prices
  in  if (length bef == _range_) && (length aft == _range_)
        then case pivotType of
              Low ->
                (all (> cur) bef) && (all (> cur) aft)
              High ->
                (all (< cur) bef) && (all (< cur) aft)
        else False