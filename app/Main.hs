import Bot
import Types
import PivotIndicator

import Data.Time.Clock.POSIX(POSIXTime, getPOSIXTime)

-------------------------------------------------------------------------------

predicate :: POSIXTime -> Bool
predicate time =
    let mins = (truncate time) `div` 60 `mod` 60
        hours = (truncate time) `div` 3600 `mod` 24
    in  (hours >= 3 && hours <= 18) && (mins == 5 || mins == 10 || mins == 15)


transCheck :: POSIXTime -> Bool
transCheck = (== 1).(`mod` 60).(`div` 60).truncate


expiryDateCalculator :: IO Int
expiryDateCalculator = ((* 3600).succ.(`div` 3600).truncate) <$> getPOSIXTime


main :: IO ()
main = do
    let conf = Config { amount         = 10
                      , basis          = Stake
                      , expiryDateCalc = expiryDateCalculator }
                      
        strategy = Strategy { isBuying = predicate
                            , succDelay     = 60000000
                            , failDelay     = 1000000
                            , indicator     = pivotIndicator
                            , symbols       = ["frxUSDJPY", "frxEURUSD", "frxGBPUSD", "frxGBPJPY", "frxAUDUSD", "frxAUDJPY", "frxNZDUSD", "frxEURAUD", "frxEURNZD"]
                            , config        = conf
                            , isTransactionCheck = transCheck }

    bot strategy